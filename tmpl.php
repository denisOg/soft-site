<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>media/css/style.css"/>
    <script src="<?=BASE_URL?>media/js/script.js" type="text/javascript"></script>
    <script src="<?=BASE_URL?>media/js/jquery-1.4.4.js" type="text/javascript"></script>
    <script src="<?=BASE_URL?>media/js/ajax_upload.js" type="text/javascript"></script>
</head>
<body>
<script>
    var SITE_URL = '<?=SITE_URL?>';
    var BASE_URL = '<?=BASE_URL?>';
</script>
<div id="l_bg">
    <div id="r_bg">
        <table id="main_table">
            <tr>
                <td class="td1" valign="top">
                    <!--left menu-->
                    Привет, <b style="color: blue;"><?=isAdmin('bool')?'Администратор':'Пользователь';?></b>
                    <ul id="left_menu">
                        <? if (!isAdmin('bool'))
                    { ?>
                        <li><a href="<?=SITE_URL?>index.php?page=list"">Список</a></li>
                        <li><a href="<?=SITE_URL?>index.php?page=admin_login"">Админ панель</a></li>
                        <? } else
                    { ?>
                        <li><a href="<?=SITE_URL?>index.php?page=admin_list"">Список</a></li>
                        <li><a href="<?=SITE_URL?>index.php?page=admin_add_edit">Добавить</a></li>
                        <li><a href="<?=SITE_URL?>index.php?page=do_logout"">Выход</a></li>
                        <? }?>
                    </ul>

                </td>
                <td class="td2">
                    <div id="logo">
                        <img src="<?=BASE_URL?>/media/img/1.jpg" alt="logo">
                    </div>
                    <!--Content-->
                    <?=!empty($content) ? $content : 'empty_content'?>
                </td>
                <td class="td3">
                   &nbsp;
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>