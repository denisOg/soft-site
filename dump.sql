-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 12 2013 г., 23:11
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `fonotov`
--

-- --------------------------------------------------------

--
-- Структура таблицы `license`
--

CREATE TABLE IF NOT EXISTS `license` (
  `id_license` int(11) NOT NULL AUTO_INCREMENT,
  `license` varchar(500) NOT NULL,
  PRIMARY KEY (`id_license`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `license`
--

INSERT INTO `license` (`id_license`, `license`) VALUES
(1, 'FreeWare '),
(2, 'ShareWare'),
(3, 'Demo'),
(4, 'Commercial'),
(5, 'Donationware'),
(6, 'Adware');

-- --------------------------------------------------------

--
-- Структура таблицы `soft`
--

CREATE TABLE IF NOT EXISTS `soft` (
  `id_soft` int(11) NOT NULL AUTO_INCREMENT,
  `soft` varchar(500) NOT NULL,
  `desc_soft` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `license_id` int(11) NOT NULL,
  `system_soft` varchar(250) NOT NULL,
  `file_soft` varchar(50) NOT NULL,
  PRIMARY KEY (`id_soft`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `soft`
--

INSERT INTO `soft` (`id_soft`, `soft`, `desc_soft`, `type_id`, `license_id`, `system_soft`, `file_soft`) VALUES
(15, 'ArtScope 0.98', 'ArtScope - программа для развлечения и отдыха, позволяющая создавать волшебные образов. Отдохните от суеты и спешки, и насладитесь восхитительным, постоянно меняющимся декоративным калейдоскопом. Подарите вашим глазам яркие и насыщенные краски, расслабляясь и радуясь движению узоров и цветовых комбинаций, медленно вращающихся в завораживающем танце света. Добавьте музыкальное сопровождение и эффект будет еще более сильным.', 2, 6, '["1","4"]', '820c210242f610afc791c98d8105a83b.zip'),
(17, 'TraderStar 1.1.0.2', 'TraderStar - финансовый веб-браузер с уникальными технологиями технического анализа и разработки собственных торговых стратегий, предлагающий бесплатный серфинг в море бирж. Будет полезен трейдерам, для проведения технического анализа высокого уровня; обычным пользователям, желающим обеспечить себе финансовое благополучие и стабильность; программистам, создающим на языке Java-script свои индикаторы; учебным центрам, для обучения будущих финансистов; журналистам-обозревателям, для более точной оценки происходящих событий в мире бирж и многим многим другим.', 6, 5, '["7"]', '6aa6c2c9832b3167d6077426fa8ce812.zip'),
(18, 'AkelPad 4.8.3', 'kelPad - текстовый редактор с открытым исходным кодом, созданный, чтобы быть маленьким и быстрым. Может служить альтернативой стандартного Блокнота, входящего в состав Windows.', 1, 2, '["4","5","3"]', '7d66daa05aaa02d232516909498102fe.zip');

-- --------------------------------------------------------

--
-- Структура таблицы `system`
--

CREATE TABLE IF NOT EXISTS `system` (
  `id_system` int(11) NOT NULL AUTO_INCREMENT,
  `system` varchar(500) NOT NULL,
  `date_system` date NOT NULL,
  `company_system` varchar(200) NOT NULL,
  PRIMARY KEY (`id_system`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `system`
--

INSERT INTO `system` (`id_system`, `system`, `date_system`, `company_system`) VALUES
(1, 'Linux', '2013-04-01', 'Линус Торвалдс'),
(2, 'DOS', '0000-00-00', 'Tim Paterson'),
(3, 'Microsoft Windows XP', '0000-00-00', 'Microsoft '),
(4, 'Microsoft Windows 7', '0000-00-00', 'Microsoft '),
(5, 'Microsoft Windows 8', '0000-00-00', 'Microsoft '),
(6, 'Windows Vista', '0000-00-00', 'Microsoft '),
(7, 'Mac OS X', '0000-00-00', 'Apple'),
(8, 'Ubuntu', '0000-00-00', 'Ubuntu');

-- --------------------------------------------------------

--
-- Структура таблицы `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `type`
--

INSERT INTO `type` (`id_type`, `type`) VALUES
(1, 'Текстовые редакторы'),
(2, 'Графические редакторы и средства мультимедиа'),
(3, 'Электронные таблицы и базы данных'),
(4, 'Телекоммуникационные программы'),
(5, 'Вспомогательные программы (утилиты)'),
(6, 'Специализированные программы'),
(7, 'Компьютерные игры'),
(8, 'IDE');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
