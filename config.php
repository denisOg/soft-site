<?php
/*DB*/
$MYSQL_NAME = 'fonotov';
$MYSQL_USER = 'root';
$MYSQL_PASS = '';
$MYSQL_HOST = 'localhost';
define('BASE_URL','http://'.$_SERVER['SERVER_NAME'].'/');
define('SITE_URL','http://'.$_SERVER['SERVER_NAME'].'/');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

define('LOGIN_ADMIN','admin');
define('PASS_ADMIN','1111111');
session_start();

include_once(__DIR__.'/classes/class.MySQL.php');
$oMySQL = new MySQL($MYSQL_NAME, $MYSQL_USER, $MYSQL_PASS, $MYSQL_HOST);

function render($path=null,$data=array())
{

    if(!$path) die('error path');
    foreach ($data as $key => $value)
    {
        $$key=$value;
        
    }
    ob_start();
    include_once(realpath('.').'/tmpl/'.$path);
    $content = ob_get_contents();
    ob_end_clean();
    include_once(__DIR__.'/tmpl.php');

}

/**
 * @param string $type  bool || redirect|| die
 */
function isAdmin($type='bool',$url=null)
{
    $result=false;
    if((!empty($_SESSION['LOGIN_ADMIN']) && $_SESSION['LOGIN_ADMIN']==LOGIN_ADMIN) && !empty($_SESSION['PASS_ADMIN']) && $_SESSION['PASS_ADMIN']==PASS_ADMIN)
        $result =  TRUE;
    else
       $result = false;

    if($type=='bool')
        return  $result; //die('Access is forbidden.');

    if($type=='die')
        if(!$result) die('Access is forbidden.');

    if($type=='redirect' && $url)
        header("Location: ".$url);

}
function  get_array($key = null, $value = null, $array = array())
{


    if (!$key || !$value || !$array || !is_array($array)) return FALSE;


    $check_param = (array)($array[0]);
    /*
    if (isset($check_param[$value])){
		prd($check_param);
	}*/
    if (!isset($check_param[$key]) || !isset($check_param[$value])) return FALSE;


    $result = array();

    foreach ($array as $item)
    {
        $item = (array)$item;
        $result[$item[$key]] = $item[$value];
    }

    return $result;


}

function viewSystem($param=null,$aSystem=array())
{

    $result='';
    if(!$param || !$aSystem) return $result;
    $aParam = json_decode($param);
    if(empty($aParam) || !is_array($aParam)) return $result;
    foreach ($aParam as $key => $value)
    {
        $result .= (!empty($aSystem[$value]))?$aSystem[$value].'<br>':'';
    }
    return $result;
}

function getPhoto($param=null)
{
    return (is_file(realpath('.').'/media/upload/'.$param))?BASE_URL.'/media/upload/'.$param:BASE_URL.'/media/upload/nophoto.png';
}
function prn($content)
{
    echo '<pre style="background: lightgray; border: 1px solid black;">';
    print_r($content);
    echo '</pre>';
}