<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'On');
ini_set('short_open_tag', 'On');
include_once(__DIR__ . '/config.php');
$PAGE = !empty($_GET['page']) ? $_GET['page'] : 'list';
$data = array('test' => 'Success');
$data['aSystem'] = get_array('id_system', 'system', $oMySQL->ExecuteSQL('SELECT * FROM system  ORDER BY system'));
$data['aType'] = get_array('id_type', 'type', $oMySQL->ExecuteSQL('SELECT * FROM type  ORDER BY type'));
$data['aLic'] = get_array('id_license', 'license', $oMySQL->ExecuteSQL('SELECT * FROM license  ORDER BY license'));

if ($PAGE == 'list')
{
    $data['title'] = 'Список программ';
    $data['aResult'] = $oMySQL->Select('soft');
    render('_list.php', $data);
}

if ($PAGE == 'admin_list')
{
    isAdmin();
    $data['title'] = 'Список программ';
    $data['aResult'] = $oMySQL->Select('soft');
    render('_admin_list.php', $data);
}

if ($PAGE == 'admin_add_edit')
{
    isAdmin('die');
    $data['title'] = 'Добавить программу';
    if (!empty($_GET['id']))
    {
        $data['aResult'] = $oMySQL->Select('soft', array('id_soft' => (int)$_GET['id']));
        $data['aResult'] = (is_array($data['aResult'])) ? $data['aResult'][0] : NULL;
    }
    render('_admin_form.php', $data);
}

if ($PAGE == 'save')
{
    isAdmin();
    if (IS_AJAX)
    {
        $sError = '';
        $aResult = array('status' => 0, 'text' => 'Ошибки');
        $Input['soft'] = !empty($_POST['soft']) ? addslashes(strip_tags(trim($_POST['soft']))) : NULL;
        $Input['desc_soft'] = !empty($_POST['desc_soft']) ? addslashes(strip_tags(trim($_POST['desc_soft']))) : NULL;
        $Input['type_id'] = !empty($_POST['type_id']) ? (int)$_POST['type_id'] : NULL;
        $Input['license_id'] = !empty($_POST['license_id']) ? (int)$_POST['license_id'] : NULL;
        $Input['file_soft'] = !empty($_POST['file_soft']) ? $_POST['file_soft'] : NULL;
        $Input['system_soft'] = !empty($_POST['system_soft']) ? $_POST['system_soft'] : NULL;

        $id_soft = !empty($_POST['id_soft']) ? (int)$_POST['id_soft'] : NULL;
        if (strlen($Input['soft']) < 2) $sError .= '<b>Укажите название<b><br>';
        if (strlen($Input['file_soft']) < 2) $sError .= '<b>Загрузите архив<b><br>';
        if (strlen($Input['desc_soft']) < 2) $sError .= '<b>Укажите описание<b><br>';
        if ($Input['type_id'] == 0) $sError .= '<b>Укажите тип<b><br>';
        if ($Input['license_id'] == 0) $sError .= '<b>Укажите условия распространения<b><br>';
        if ($Input['system_soft'] == 0) $sError .= '<b>Опрерационные системы<b><br>';
        $aResult['error'] = $sError;
        if ($sError == '')
        {
            $Input['system_soft'] = json_encode($Input['system_soft']);
            //ошибок нет
            if (!empty($id_soft))
            {
                //обновление
                $aResult['text'] = 'Обновление прошло успешно';
                $oMySQL->Update('soft', $Input, array('id_soft' => $id_soft));

            } else
            {
                //добавление
                $aResult['text'] = 'Добавление прошло успешно';
                $oMySQL->Insert($Input, 'soft');


            }
            $aResult['status'] = 1;
        }
        echo json_encode($aResult);
        return;

    }
}


if ($PAGE == 'admin_delete')
{
    isAdmin('die');
    if (IS_AJAX)
    {
        $id = (int)$_POST['id'];
        $oMySQL->Delete('soft', array('id_soft' => $id));
        echo 'ok';
    }

}
if ($PAGE == 'loadFile')
{
    isAdmin('die');
    $file_name = md5(time() + mt_rand(1, 1000000000)) . '.zip';
    $file = realpath('.') . '/media/upload/' . $file_name;
    if (move_uploaded_file($_FILES['file_soft']['tmp_name'], $file))
    {
        echo  $file_name;
    } else
    {
        echo "error";
    }


}


if ($PAGE == 'admin_login')
{
    if (!isAdmin('bool')) render('_login_form.php', $data);

}
if ($PAGE == 'do_login')
{
    $aResult = array('status' => 0, 'error' => '');
    $Input['login'] = !empty($_POST['login']) ? addslashes(strip_tags(trim($_POST['login']))) : NULL;
    $Input['pass'] = !empty($_POST['pass']) ? addslashes(strip_tags(trim($_POST['pass']))) : NULL;
    if (empty($Input['login'])) $aResult['error'] .= 'Поле логин обязательно для заполнения<br>';
    if (empty($Input['pass'])) $aResult['error'] .= 'Поле пароль обязательно для заполнения<br>';


    if ($Input['login'] !== LOGIN_ADMIN) $aResult['error'] .= 'Нет такого логина<br>';
    if ($Input['pass'] !== PASS_ADMIN) $aResult['error'] .= 'Нет такого пароля<br>';

    if ($aResult['error'] == '')
    {
        //авторизирую
        $_SESSION['LOGIN_ADMIN'] = LOGIN_ADMIN;
        $_SESSION['PASS_ADMIN'] = PASS_ADMIN;
        $aResult['text'] = 'Авторизация прошла успешно';
        $aResult['status'] = 1;

    }
    echo json_encode($aResult);


}
if ($PAGE == 'do_logout')
{
    if (isAdmin('die')) unset($_SESSION);
    session_destroy();
    header("Location: " . SITE_URL);

}
if ($PAGE == 'view')
{
    $id = (int)$_GET['id'];
    $data['aResult'] = $oMySQL->SELECT('soft', array('id_soft' => $id));
    $data['aResult'] = (is_array($data['aResult'])) ? $data['aResult'][0] : NULL;
    render('_view.php', $data);


}