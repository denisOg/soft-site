<h2>Админ панель: <?php
    echo !empty($title) ? $title : '';
    ?></h2>

<form  id="form" action="<?SITE_URL?>index.php?page=admin_add_edit&id=<?=!empty($aResult['id_soft']) ? $aResult['id_soft'] : ''?>"
      method="post" onsubmit="return false">
    <div id="error_form"></div>
    <input type="hidden" name="id_soft" value="<?=!empty($aResult['id_soft']) ? $aResult['id_soft'] : ''?>">
    <input type="hidden" name="valid_me" value="valid_me">
    <lable>Название</lable><br>
    <input type="text" name="soft" value="<?=!empty($aResult['soft']) ? $aResult['soft'] : ''?>">
    <br>
    <br>
    <lable>Описание</lable><br>
    <textarea name="desc_soft" id="" cols="30"
              rows="10"><?=!empty($aResult['desc_soft']) ? $aResult['desc_soft'] : ''?></textarea>
    <br>
    <br>

    <lable>Архив(только .zip)</lable><br>
    <div id="file_soft" class="">Загрузить</div>
    <div id="result_file_soft">
        <?if (!empty($aResult['file_soft']))
    {?>
        Архив:<?=$aResult['file_soft']?>

    <?}?>
    </div>
    <input type="hidden" name="file_soft" value="<?=!empty($aResult['file_soft']) ? $aResult['file_soft'] : ''?>">
    <br>
    <br>

    <lable>Тип лицензии</lable><br>
    <?foreach ($aLic as $key => $value)
{
    ?>
    <input type="radio" name="license_id"
           value="<?=$key?>" <?=(isset($aResult['license_id']) && $aResult['license_id'] == $key) ? 'checked' : ''?>> <?= $value ?><br>
    <? } ?>

    <br>
    <br>
    <lable>Тип</lable><br>
    <select name="type_id" id="">
        <?foreach ($aType as $key => $value)
    {
        ?>
        <option
            value="<?=$key?>" <?=(isset($aResult['type_id']) && $aResult['type_id'] == $key) ? 'selected' : ''?> ><?=$value?></option>
        <? } ?>
    </select>
    <br>
    <br>

    <lable>Операционные системы</lable><br>
    <select name="system_soft[]" multiple  id="">
        <?
        $aUserSystem = (!empty($aResult['system_soft'])) ? json_decode($aResult['system_soft']) : NULL;
        foreach ($aSystem as $key => $value)
        {
            ?>
            <option
                value="<?=$key?>" <?=(isset($aUserSystem) && in_array($key, $aUserSystem)) ? 'selected' : ''?> ><?=$value?></option>
            <? } ?>
    </select>
    <br>
    <br>

    <input type="submit" onclick="save()" value="Сохранить">

</form>
<script type="text/javascript">
    $(function(){
        var btnUpload=$('#file_soft');
        var status=$('#result_file_soft');
        new AjaxUpload(btnUpload, {
            action: SITE_URL + 'index.php?page=loadFile',
//Имя файлового поля ввода
            name: 'file_soft',
            onSubmit: function(file, ext){
                if (! (ext && /^(zip)$/.test(ext))){
// Валидация расширений файлов
                    status.text('Только ZIP файлы');
                    return false;

                }
                status.text('Загрузка...');
            },
            onComplete: function(file, response){
//Очищаем текст статуса
                status.text('');
//Добавляем загруженные файлы в лист
                if(response==="error"){
                    alert('Ошибка при загрузке');
                } else{
                    status.text('Архив: '+response);
                    $('input[name="file_soft"]').val(response);

                }
            }
        });
    });

</script>
